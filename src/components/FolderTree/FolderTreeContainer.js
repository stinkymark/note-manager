import React from 'react';
import { connect } from 'dva';

import FolderTree from './FolderTree.js';

const mapStateToProps = (state) => {
  return {
    folders: state.app.data.folders
  }
}

const mapDispatchToProps = (dispatch) => {
  dispatch({type: 'app/loaddirectories'});
  // dispatch({
  //   type: 'app/createDirectory',
  //   meta: {
  //     parentId: 'parentId',
  //     name: 'some name'
  //   }
  // });
  // dispatch({
  //   type: 'app/updateDirectory',
  //   meta: {
  //     id: 'idOfCurrentDirrectory',
  //     parentId: 'parentId',
  //     name: 'some name'
  //   }
  // });
  // dispatch({
  //   type: 'app/deleteDirectory',
  //   meta: {
  //     id: 'someid'
  //   }
  // });
  // dispatch({type: 'app/loadNotices'});
  // dispatch({
  //   type: 'app/createNotice',
  //   meta: {
  //     directoryId: 'someDirectoryId',
  //     title: 'title of the notice',
  //     description: 'description of the notice',
  //     tags: ['some tag', 'some another tag']
  //   }
  // });
  // dispatch({
  //   type: 'app/deleteNotice',
  //   meta: {
  //     notice: 'id'
  //   }
  // });
  // dispatch({type: 'app/updateNotice'});
  return {
    loaddirectories: console.log
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FolderTree);
