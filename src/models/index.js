import {
  loaddirectories, createDirectory, updateDirectory, deleteDirectory,
  loadNotices, createNotice, updateNotice, deleteNotice
} from 'services';

export default {
  namespace: 'app',

  state: {
    data: {
      folders: [1,2,3,4],
      notices: []
    },
    requests: [],
    view: {
      sidebar: false
    }
  },
  effects: {
    *loaddirectories(action, {call, put}) {
      const data = yield call(loaddirectories);
      yield put({
        type: 'setDirectoriesList',
        payload: data
      })
    },
    *createDirectory(action, {call, put}) {
      const data = yield call(createDirectory);
    },
    *updateDirectory(action, {call, put}) {
      const data = yield call(updateDirectory);
    },
    *deleteDirectory(action, {call, put}) {
      const data = yield call(deleteDirectory);
    },
    *loadNotices(action, {call, put}) {
      const data = yield call(loadNotices);
    },
    *createNotice(action, {call, put}) {
      const data = yield call(createNotice);
    },
    *updateNotice(action, {call, put}) {
      const data = yield call(updateNotice);
    },
    *deleteNotice(action, {call, put}) {
      const data = yield call(deleteNotice);
    },
  },
  reducers: {
    setDirectoriesList(state, {payload}) {
      console.log('asdasdasdas', payload);
      return state;
    }
  },

}
