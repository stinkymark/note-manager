import request from 'utils/request';
import { stringify } from 'qs';

export async function loaddirectories() {
  return request(`/api/directories`);
}

export async function createDirectory(directory) {
  return request(`/api/directories`, {
    method: 'POST',
    body: stringify(directory)
  })
}

export async function updateDirectory(directory) {
  return request(`/api/directories`, {
    method: 'PUT',
    body: stringify(directory)
  })
}

export async function deleteDirectory(directoryId) {
  return request(`/api/directories`, {
    method: 'DELETE',
    body: stringify({id: directoryId})
  })
}

export async function loadNotices() {
  return request(`/api/notices`);
}

export async function createNotice(notice) {
  return request(`/api/notices`, {
    method: 'post',
    body: stringify(notice)
  })
}

export async function updateNotice(notice) {
  return request(`/api/notices`, {
    method: 'PUT',
    body: stringify(notice)
  })
}

export async function deleteNotice(noticeId) {
  return request(`/api/notices`, {
    method: 'DELETE',
    body: stringify({id: noticeId})
  })
}
