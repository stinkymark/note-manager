'use strict';

module.exports = {

  'GET /api/directories': function (req, res) {
    setTimeout(function () {
      res.json({
        success: true,
        data: ['foo', 'bar'],
      });
    }, 500);
  },
  'POST /api/directories': function (req, res) {
    setTimeout(function () {
      res.json({
        success: true,
        data: ['foo', 'bar'],
      });
    }, 500);
  },
  'PUT /api/directories': function (req, res) {
    setTimeout(function () {
      res.json({
        success: true,
        data: ['foo', 'bar'],
      });
    }, 500);
  },
  'DELETE /directories': function(req, res) {
    setTimeout(function () {
      res.json({
        success: true
      });
    }, 500);
  },
  'GET /api/notices': function (req, res) {
    setTimeout(function () {
      res.json({
        success: true,
        data: ['foo', 'bar'],
      });
    }, 500);
  },
  'POST /api/notices': function (req, res) {
    setTimeout(function () {
      res.json({
        success: true,
        data: ['foo', 'bar'],
      });
    }, 500);
  },
  'PUT /api/notices': function (req, res) {
    setTimeout(function () {
      res.json({
        success: true,
        data: ['foo', 'bar'],
      });
    }, 500);
  },
  'DELETE /notices': function(req, res) {
    setTimeout(function () {
      res.json({
        success: true
      });
    }, 500);
  },

};
